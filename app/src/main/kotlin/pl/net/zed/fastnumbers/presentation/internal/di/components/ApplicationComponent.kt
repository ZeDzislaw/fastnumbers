package pl.net.zed.fastnumbers.presentation.internal.di.components

import android.content.Context
import dagger.Component
import pl.net.zed.fastnumbers.domain.executor.PostExecutionThread
import pl.net.zed.fastnumbers.domain.executor.ThreadExecutor
import pl.net.zed.fastnumbers.domain.repository.NumberRepository
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ApplicationModule
import javax.inject.Singleton

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    //Exposed to sub-graphs.
    fun context(): Context

    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
    fun numbersRepository(): NumberRepository
}