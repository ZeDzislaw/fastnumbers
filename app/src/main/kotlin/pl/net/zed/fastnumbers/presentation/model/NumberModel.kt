package pl.net.zed.fastnumbers.presentation.model

import java.util.*

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
data class NumberModel(val name: String, val number: String, val addedAt: Date = Date())