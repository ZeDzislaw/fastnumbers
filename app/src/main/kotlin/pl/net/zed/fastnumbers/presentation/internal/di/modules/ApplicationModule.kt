package pl.net.zed.fastnumbers.presentation.internal.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.net.zed.fastnumbers.data.executor.JobExecutor
import pl.net.zed.fastnumbers.data.repository.NumberDataRepository
import pl.net.zed.fastnumbers.domain.executor.PostExecutionThread
import pl.net.zed.fastnumbers.domain.executor.ThreadExecutor
import pl.net.zed.fastnumbers.domain.repository.NumberRepository
import pl.net.zed.fastnumbers.presentation.FastNumbersApp
import pl.net.zed.fastnumbers.presentation.UIThread
import javax.inject.Singleton


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
class ApplicationModule(private val application: FastNumbersApp) {

    @Provides
    @Singleton
    internal fun provideApplicationContext(): Context = this.application

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    internal fun provideNumberRepository(userDataRepository: NumberDataRepository): NumberRepository = userDataRepository
}