package pl.net.zed.fastnumbers.presentation

import android.app.Application
import pl.net.zed.fastnumbers.presentation.internal.di.components.ApplicationComponent
import pl.net.zed.fastnumbers.presentation.internal.di.components.DaggerApplicationComponent
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ApplicationModule
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
class FastNumbersApp : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initInjector()
        FlowManager.init(FlowConfig.Builder(this).openDatabasesOnInit(true).build())
    }

    private fun initInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    fun appComponent(): ApplicationComponent = applicationComponent
}