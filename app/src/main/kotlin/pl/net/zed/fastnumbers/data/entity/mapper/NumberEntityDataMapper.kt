package pl.net.zed.fastnumbers.data.entity.mapper

import pl.net.zed.fastnumbers.data.entity.NumberEntity
import pl.net.zed.fastnumbers.domain.Number
import pl.net.zed.fastnumbers.domain.util.Mapper
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Damian Zawadzki on 18.01.2017.
 */
/**
 * Mapper class used to transform [NumberEntity] (in the data layer) to [Number] in the
 * domain layer.
 */
@Singleton class NumberEntityDataMapper
@Inject internal constructor() : Mapper<NumberEntity, Number> {

    /**
     * Maps a [NumberEntity] into an [Number].
     * @param obj object to be transformed.
     *
     * @return [Number] if valid [NumberEntity].
     */
    override fun map(obj: NumberEntity): Number {
        return Number(obj.numberId, obj.name, obj.number, obj.creationDate)
    }

    /**
     * Maps a List of [NumberEntity] into a Collection of [Number].
     * @param collection objects Collection to be transformed.
     *
     * @return [Number] if valid [NumberEntity].
     */
    override fun map(collection: Collection<NumberEntity>): Collection<Number> {
        return collection.map { map(it) }
    }
}