package pl.net.zed.fastnumbers.presentation.presenter

import pl.net.zed.fastnumbers.domain.Number
import pl.net.zed.fastnumbers.domain.interactor.DefaultObserver
import pl.net.zed.fastnumbers.domain.interactor.GetNumberListCase
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import pl.net.zed.fastnumbers.presentation.mapper.NumberModelDataMapper
import pl.net.zed.fastnumbers.presentation.view.MainActivityView
import javax.inject.Inject


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
@PerActivity class MainActivityPresenter
@Inject constructor(val getNumberListCase: GetNumberListCase,
                    val numberModelDataMapper: NumberModelDataMapper)
    : BasePresenter<MainActivityView>(), IMainActivityPresenter {

    override fun init() {
        startLoadingWholeList()
    }

    override fun startLoadingWholeList() {
        showLoading()
        getNumberListCase.execute(NumberListObserver(), null)
    }

    override fun showNumberList(numberList: List<Number>) {
        val transformedNumberList = numberModelDataMapper.transform(numberList)
        doIfViewAttached { it.showListOfNumbers(transformedNumberList) }
    }

    override fun showLoading(showLoading: Boolean) {
        doIfViewAttached {
            it.showLoading(showLoading)
            it.lockViews(showLoading)
        }
    }

    override fun detachView(retainInstance: Boolean) {
        getNumberListCase.dispose()
        super.detachView(retainInstance)
    }

    inner class NumberListObserver : DefaultObserver<List<Number>>() {

        override fun onComplete() {
            //this@UserListPresenter.hideViewLoading()
            showLoading(false)
        }

        override fun onError(exception: Throwable) {
            /*this@UserListPresenter.hideViewLoading()
            this@UserListPresenter.showErrorMessage(DefaultErrorBundle(e as Exception))
            this@UserListPresenter.showViewRetry()*/
        }

        override fun onNext(t: List<Number>) {
            showNumberList(t)
        }
    }
}