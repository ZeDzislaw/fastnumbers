package pl.net.zed.fastnumbers.domain.repository

import io.reactivex.Single
import pl.net.zed.fastnumbers.domain.Number

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
interface NumberRepository {
    fun getAllNumbers(): Single<List<Number>>
    fun getNumber(id: Long): Single<Number>
}