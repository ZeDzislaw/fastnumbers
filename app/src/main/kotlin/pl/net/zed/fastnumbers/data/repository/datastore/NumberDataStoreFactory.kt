package pl.net.zed.fastnumbers.data.repository.datastore

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Damian Zawadzki on 18.01.2017.
 */
@Singleton
class NumberDataStoreFactory @Inject constructor(val context: Context) {

    fun createLocalDataStore(): NumberDataStore = LocalNumberDataStore()

    //TODO unnecessary ?
    fun create(id: Long): NumberDataStore {
        return LocalNumberDataStore()
    }
}