package pl.net.zed.fastnumbers.presentation.internal.di

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
/**
 * Interface representing a contract for clients that contains a component for dependency injection.
 */
interface HasComponent<C> {
    val component: C
}