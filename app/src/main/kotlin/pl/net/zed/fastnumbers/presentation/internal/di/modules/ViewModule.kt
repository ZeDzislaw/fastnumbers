package pl.net.zed.fastnumbers.presentation.internal.di.modules

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import javax.inject.Qualifier

/**
 * Created by Lucas on 2017-01-19.
 */
@Module
class ViewModule(private val activityContext: Context) {

    @Provides @PerActivity fun provideSpinnerProgressDialog(): ProgressDialog {
        return ProgressDialog(activityContext)
    }

    @Provides @PerActivity @BarProgress fun provideProgressDialog(): ProgressDialog {
        return with(ProgressDialog(activityContext)) {
            setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            this
        }
    }

}

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class SpinnerProgress

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BarProgress