package pl.net.zed.fastnumbers.data.repository.datastore.db

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.kotlinextensions.modelAdapter
import com.raizlabs.android.dbflow.structure.Model

/**
 * Created by Lucas on 2017-01-30.
 */
@Table(name = "Numbers", database = FastNumbersDb::class) data class NumberModel
constructor(@PrimaryKey(autoincrement = true) var numberId: Long = 0,
                    @Column var name: String = "",
                    @Column var number: String = "",
                    @Column var creationDate: Long = 0) : Model {

    override fun insert(): Long = modelAdapter<NumberModel>().insert(this)

    override fun save(): Boolean = modelAdapter<NumberModel>().save(this)

    override fun update(): Boolean = modelAdapter<NumberModel>().update(this)

    override fun delete(): Boolean = modelAdapter<NumberModel>().delete(this)

    override fun exists(): Boolean = modelAdapter<NumberModel>().exists(this)

    override fun load() = modelAdapter<NumberModel>().load(this)
}