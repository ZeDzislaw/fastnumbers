package pl.net.zed.fastnumbers.presentation.presenter

import pl.net.zed.fastnumbers.domain.Number

/**
 * Created by Lucas on 2017-01-18.
 */
interface IMainActivityPresenter {
    fun startLoadingWholeList()
    fun showNumberList(numberList: List<Number>)
    fun showLoading(showLoading: Boolean = true)
}