package pl.net.zed.fastnumbers.presentation.mapper

import pl.net.zed.fastnumbers.domain.Number
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import pl.net.zed.fastnumbers.presentation.model.NumberModel
import java.util.*
import javax.inject.Inject

/**
 * Created by Damian Zawadzki on 17.01.2017.
 *
 *
 * Mapper class used to transform model (in the domain layer) to model in the
 * presentation layer.
 *
 */
@PerActivity
class NumberModelDataMapper @Inject constructor() {

    fun transform(number: Number): NumberModel {
        val mappedNumber = NumberModel(number.name, number.number, number.creationDate)
        return mappedNumber
    }

    fun transform(numberList: List<Number>?): List<NumberModel> {
        val mappedList = ArrayList<NumberModel>()
        numberList?.let { list ->
            list.forEach { mappedList.add(transform(it)) }
        }
        return mappedList
    }

}