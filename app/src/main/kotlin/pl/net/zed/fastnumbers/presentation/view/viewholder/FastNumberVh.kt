package pl.net.zed.fastnumbers.presentation.view.viewholder

import android.view.View
import android.widget.TextView
import pl.net.zed.fastnumbers.R
import pl.net.zed.fastnumbers.presentation.model.NumberModel

/**
 * Created by Lucas on 2017-01-18.
 */
class FastNumberVh(itemView: View) : BaseFastNumberViewHolder(itemView) {

    val titleText by lazy { itemView.findViewById(R.id.fast_number_title) as TextView }
    val subtitleText by lazy { itemView.findViewById(R.id.fast_number_subtitle) as TextView }

    override fun onBind(item: NumberModel) {
        titleText.text = item.name.trim()
        subtitleText.text = item.number.trim()
    }

    override fun onRecycle() {
        // not implemented
    }

    companion object {
        fun getLayoutId(): Int = R.layout.item_fast_number
    }
}