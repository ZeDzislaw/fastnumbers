package pl.net.zed.fastnumbers.data.entity

import java.util.*

/**
 * Created by Damian Zawadzki on 18.01.2017.
 */
data class NumberEntity(val numberId: Long, val name: String, val number: String, val creationDate: Date)