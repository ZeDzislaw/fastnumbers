package pl.net.zed.fastnumbers.presentation.navigation

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */


/**
 * Class used to navigate through the application.
 */
@Singleton class Navigator
@Inject constructor()//empty
{

    /**
     * Goes to the user list screen.

     * @param context A Context needed to open the destiny activity.
     */
    fun navigateToNumberList(context: Context) {
        if (context != null) {/*
            val intentToLaunch = MainActivity.getCallingIntent(context)
            context!!.startActivity(intentToLaunch)*/
        }
    }

 /*   *//**
     * Goes to the user details screen.

     * @param context A Context needed to open the destiny activity.
     *//*
    fun navigateToUserDetails(context: Context, userId: Int) {
        if (context != null) {
            val intentToLaunch = UserDetailsActivity.getCallingIntent(context, userId)
            context!!.startActivity(intentToLaunch)
        }
    }*/
}
