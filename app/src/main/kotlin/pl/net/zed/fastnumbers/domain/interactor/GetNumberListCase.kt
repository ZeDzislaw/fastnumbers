package pl.net.zed.fastnumbers.domain.interactor

import io.reactivex.Observable
import pl.net.zed.fastnumbers.domain.Number
import pl.net.zed.fastnumbers.domain.executor.PostExecutionThread
import pl.net.zed.fastnumbers.domain.executor.ThreadExecutor
import pl.net.zed.fastnumbers.domain.repository.NumberRepository
import javax.inject.Inject


/**
 * Created by Damian Zawadzki on 17.01.2017.
 *
 * This class is an implementation of [UseCase] that represents a use case for
 * retrieving a collection of all [Number].
 */
class GetNumberListCase
@Inject constructor(private val numberRepository: NumberRepository,
                    threadExecutor: ThreadExecutor,
                    postExecutionThread: PostExecutionThread)
    : UseCase<List<Number>, Void>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Void?): Observable<List<Number>> {
        //TODO handle params - like search filter stuff
        return numberRepository.getAllNumbers().toObservable()
    }

}