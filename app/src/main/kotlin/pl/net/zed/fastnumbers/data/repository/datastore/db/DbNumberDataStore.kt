package pl.net.zed.fastnumbers.data.repository.datastore.db

import com.raizlabs.android.dbflow.kotlinextensions.*

import io.reactivex.Single

import pl.net.zed.fastnumbers.data.entity.NumberEntity
import pl.net.zed.fastnumbers.data.repository.datastore.NumberDataStore
import pl.net.zed.fastnumbers.data.repository.datastore.db.NumberModel_Table.numberId

import java.util.*
import javax.inject.Inject

/**
 * Created by Lucas on 2017-01-30.
 */
class DbNumberDataStore @Inject constructor(val modelEntityDataMapper: NumberModelEntityMapper) : NumberDataStore {
    override fun numberEntityList(): Single<List<NumberEntity>> {
        val result = (select from NumberModel::class).list
                .map { modelEntityDataMapper.map(it) }
        return Single.just(result)
    }

    override fun numberEntityDetails(id: Long): Single<NumberEntity> {
        val result = (select from NumberModel::class
                where (numberId.eq(id))).result
        val entity = result?.let { modelEntityDataMapper.map(it) }
        return if (entity != null) Single.just(entity)
        else Single.error<NumberEntity>(NoSuchElementException("No element with id $id exists in database."))
    }
}