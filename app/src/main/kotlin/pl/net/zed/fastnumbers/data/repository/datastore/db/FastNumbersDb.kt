package pl.net.zed.fastnumbers.data.repository.datastore.db

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by Lucas on 2017-01-30.
 */
@Database(version = FastNumbersDb.DB_VERSION, name = FastNumbersDb.DB_NAME)
open class FastNumbersDb {
    companion object {
        const val DB_NAME = "FastNumbersDb"
        const val DB_VERSION = 1
    }
}