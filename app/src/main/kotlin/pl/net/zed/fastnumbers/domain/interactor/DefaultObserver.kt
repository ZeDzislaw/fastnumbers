package pl.net.zed.fastnumbers.domain.interactor

import io.reactivex.observers.DisposableObserver



/**
 * Created by Damian Zawadzki on 17.01.2017.
 *
 * Default [DisposableObserver] base class to be used whenever you want default error handling.
 */
abstract class DefaultObserver<T> : DisposableObserver<T>() {
    override fun onNext(t: T) {
        // no-op by default.
    }

    override fun onComplete() {
        // no-op by default.
    }

    override fun onError(exception: Throwable) {
        // no-op by default.
    }
}