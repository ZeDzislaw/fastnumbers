package pl.net.zed.fastnumbers.presentation.view.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import pl.net.zed.fastnumbers.presentation.model.NumberModel

/**
 * Created by Lucas on 2017-01-18.
 */
abstract class BaseFastNumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnBindViewHolder<NumberModel>, OnRecycleViewHolder