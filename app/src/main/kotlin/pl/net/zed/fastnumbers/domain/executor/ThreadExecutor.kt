package pl.net.zed.fastnumbers.domain.executor

import pl.net.zed.fastnumbers.domain.interactor.UseCase
import java.util.concurrent.Executor


/**
 * Created by Damian Zawadzki on 17.01.2017.
 *
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * [UseCase] out of the UI thread.
 */
interface ThreadExecutor : Executor