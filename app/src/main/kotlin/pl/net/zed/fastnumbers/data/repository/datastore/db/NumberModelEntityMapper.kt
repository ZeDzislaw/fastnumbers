package pl.net.zed.fastnumbers.data.repository.datastore.db

import pl.net.zed.fastnumbers.data.entity.NumberEntity
import pl.net.zed.fastnumbers.domain.util.Mapper
import java.util.*
import javax.inject.Inject

/**
 * Created by Lucas on 2017-01-30.
 */

class NumberModelEntityMapper
@Inject constructor() : Mapper<NumberModel, NumberEntity> {
    override fun map(obj: NumberModel): NumberEntity {
        return NumberEntity(obj.numberId, obj.name, obj.number, Date(obj.creationDate))
    }

    override fun map(collection: Collection<NumberModel>): Collection<NumberEntity> {
        return collection.map { map(it) }
    }
}