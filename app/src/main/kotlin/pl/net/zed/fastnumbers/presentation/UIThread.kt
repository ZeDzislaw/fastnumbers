package pl.net.zed.fastnumbers.presentation

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import pl.net.zed.fastnumbers.domain.executor.PostExecutionThread
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Damian Zawadzki on 18.01.2017.
 *
 * MainThread (UI Thread) implementation based on a [Scheduler]
 * which will execute actions on the Android UI thread
 */
@Singleton
class UIThread
@Inject
internal constructor() : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}