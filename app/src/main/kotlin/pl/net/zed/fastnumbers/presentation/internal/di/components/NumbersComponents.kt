package pl.net.zed.fastnumbers.presentation.internal.di.components

import dagger.Component
import pl.net.zed.fastnumbers.MainActivity
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ActivityModule
import pl.net.zed.fastnumbers.presentation.internal.di.modules.NumberModule
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ViewModule
import pl.net.zed.fastnumbers.presentation.mapper.NumberModelDataMapper
import pl.net.zed.fastnumbers.presentation.presenter.MainActivityPresenter
import pl.net.zed.fastnumbers.presentation.view.adapter.FastNumberAdapter

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class),
        modules = arrayOf(ActivityModule::class, NumberModule::class, ViewModule::class))
internal interface NumbersComponents : ActivityComponent {
    fun numberModelDataMapper(): NumberModelDataMapper
    fun mainPresenter(): MainActivityPresenter
    fun fastNumberAdapter(): FastNumberAdapter

    fun inject(baseActivity: MainActivity)
}