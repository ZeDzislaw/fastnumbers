package pl.net.zed.fastnumbers.data.repository

import io.reactivex.Single
import pl.net.zed.fastnumbers.data.entity.mapper.NumberEntityDataMapper
import pl.net.zed.fastnumbers.data.repository.datastore.NumberDataStoreFactory
import pl.net.zed.fastnumbers.domain.Number
import pl.net.zed.fastnumbers.domain.repository.NumberRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Damian Zawadzki on 18.01.2017.
 */
@Singleton class NumberDataRepository
@Inject constructor(val numberEntityDataMapper: NumberEntityDataMapper,
                    val numberDataStoreFactory: NumberDataStoreFactory) : NumberRepository {

    override fun getAllNumbers(): Single<List<Number>> {
        val numberDataStore = numberDataStoreFactory.createLocalDataStore()
        return numberDataStore.numberEntityList()
                .map { this.numberEntityDataMapper.map(it) }
                .map { it.toList() }
    }

    override fun getNumber(id: Long): Single<Number> {
        val numberDataStore = numberDataStoreFactory.create(id)
        return numberDataStore.numberEntityDetails(id)
                .map { this.numberEntityDataMapper.map(it) }
    }
}