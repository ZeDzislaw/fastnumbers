package pl.net.zed.fastnumbers

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.hannesdorfmann.mosby.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_main.*
import pl.net.zed.fastnumbers.presentation.FastNumbersApp
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import pl.net.zed.fastnumbers.presentation.internal.di.components.DaggerNumbersComponents
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ActivityModule
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ViewModule
import pl.net.zed.fastnumbers.presentation.model.NumberModel
import pl.net.zed.fastnumbers.presentation.presenter.MainActivityPresenter
import pl.net.zed.fastnumbers.presentation.view.MainActivityView
import pl.net.zed.fastnumbers.presentation.view.adapter.FastNumberAdapter
import javax.inject.Inject

class MainActivity : MvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView {

    @Inject @PerActivity lateinit var fastNumbersAdapter: FastNumberAdapter
    @Inject @PerActivity lateinit var progressDialog: ProgressDialog
    @Inject @PerActivity lateinit var mainPresenter: MainActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerNumbersComponents.builder()
                .applicationComponent((application as FastNumbersApp).appComponent())
                .activityModule(ActivityModule(this))
                .viewModule(ViewModule(this))
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)

        contactsView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        contactsView.adapter = fastNumbersAdapter
    }

    override fun createPresenter(): MainActivityPresenter = mainPresenter

    override fun showListOfNumbers(numbers: List<NumberModel>) {

    }

    override fun updateListOfNumbers(numbers: List<NumberModel>) {
    }

    override fun closeApp() {
    }

    override fun showLoading(show: Boolean) {
        if (show) progressDialog.show()
        else progressDialog.hide()
    }

    override fun lockViews(isLock: Boolean) {
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        MenuInflater(this).inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                // filter list or query new data based on filter
            }

            R.id.action_add -> {
                // show dialog with form to create new contact/number
            }

            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }
}
