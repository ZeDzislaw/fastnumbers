package pl.net.zed.fastnumbers.presentation.internal.di

import javax.inject.Scope

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
/**
 * A scoping annotation to permit objects whose lifetime should
 * conform to the life of the activity to be memorized in the
 * correct component.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity