package pl.net.zed.fastnumbers.presentation.view

import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
interface ProgressView : MvpView {
    fun showLoading(show: Boolean = true)
    fun lockViews(isLock: Boolean = true)
}