package pl.net.zed.fastnumbers.presentation.internal.di.modules

import dagger.Module

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */


/**
 * Dagger module that provides user related collaborators.
 */
@Module
class NumberModule