package pl.net.zed.fastnumbers.presentation.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.net.zed.fastnumbers.presentation.model.NumberModel
import pl.net.zed.fastnumbers.presentation.view.viewholder.BaseFastNumberViewHolder
import pl.net.zed.fastnumbers.presentation.view.viewholder.FastNumberVh
import javax.inject.Inject

/**
 * Created by Lucas on 2017-01-18.
 */
class FastNumberAdapter @Inject constructor() : RecyclerView.Adapter<BaseFastNumberViewHolder>() {

    var fastNumbersList: MutableList<NumberModel> = mutableListOf()

    override fun getItemCount(): Int = fastNumbersList.count()

    override fun onBindViewHolder(holder: BaseFastNumberViewHolder, position: Int) {
        holder.onBind(fastNumbersList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFastNumberViewHolder {
        val fastNumberItemView = LayoutInflater.from(parent.context).inflate(FastNumberVh.getLayoutId(), parent, false)
        return FastNumberVh(fastNumberItemView)
    }

    override fun onViewRecycled(holder: BaseFastNumberViewHolder?) {
        holder?.onRecycle()
        super.onViewRecycled(holder)
    }

    fun replaceData(newNumbers: MutableList<NumberModel>) {
        fastNumbersList.clear()
        fastNumbersList.addAll(newNumbers)
        notifyDataSetChanged()
    }

    fun addNumber(newNumber: NumberModel) {
        val insertPosition = fastNumbersList.count()
        fastNumbersList.add(newNumber)
        notifyItemInserted(insertPosition)
    }

    fun removeNumber(position: Int) {
        fastNumbersList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateNumber(position: Int, updatedNumber: NumberModel) {
        fastNumbersList[position] = updatedNumber
        notifyItemChanged(position)
    }
}