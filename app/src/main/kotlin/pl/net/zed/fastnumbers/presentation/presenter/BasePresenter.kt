package pl.net.zed.fastnumbers.presentation.presenter

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
abstract class BasePresenter<V : MvpView?> : MvpBasePresenter<V>() {

    override fun attachView(view: V?) {
        super.attachView(view)
        init()
    }

    fun doIfViewAttached(operation: (view: V) -> Any) {
        if (isViewAttached) view?.let { operation.invoke(it) }
    }

    abstract fun init()
}