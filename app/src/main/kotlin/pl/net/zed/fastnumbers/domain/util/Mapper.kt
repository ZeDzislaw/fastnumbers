package pl.net.zed.fastnumbers.domain.util

/**
 * Created by Lucas on 2017-01-30.
 */
interface Mapper<in From, out To> {
    fun map(obj: From): To
    fun map(collection: Collection<From>): Collection<To>
}