package pl.net.zed.fastnumbers.presentation.view

import com.hannesdorfmann.mosby.mvp.MvpView
import pl.net.zed.fastnumbers.presentation.model.NumberModel

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
interface MainActivityView : MvpView, ProgressView {

    fun showListOfNumbers(numbers: List<NumberModel>)
    fun updateListOfNumbers(numbers: List<NumberModel>)
    fun closeApp()

}