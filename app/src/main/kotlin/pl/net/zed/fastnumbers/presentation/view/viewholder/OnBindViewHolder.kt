package pl.net.zed.fastnumbers.presentation.view.viewholder

/**
 * Created by Lucas on 2017-01-18.
 */
interface OnBindViewHolder<in T> {
    fun onBind(item: T)
}