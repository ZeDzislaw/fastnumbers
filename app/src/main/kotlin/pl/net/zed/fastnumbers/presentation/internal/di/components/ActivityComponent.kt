package pl.net.zed.fastnumbers.presentation.internal.di.components

import android.app.Activity
import dagger.Component
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity
import pl.net.zed.fastnumbers.presentation.internal.di.modules.ActivityModule


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
internal interface ActivityComponent {
    //Exposed to sub-graphs.
    fun activity(): Activity
}