package pl.net.zed.fastnumbers.domain

import java.util.*

/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
data class Number(val numberId: Long, val name: String, val number: String, val creationDate: Date)