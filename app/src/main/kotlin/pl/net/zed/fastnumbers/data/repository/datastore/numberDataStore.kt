package pl.net.zed.fastnumbers.data.repository.datastore

import io.reactivex.Single
import pl.net.zed.fastnumbers.data.entity.NumberEntity

/**
 * Created by Damian Zawadzki on 18.01.2017.
 */
/**
 * Interface that represents a data store from where data is retrieved.
 */
interface NumberDataStore {
    /**
     * Get an [Single] which will emit a List of [NumberEntity].
     */
    fun numberEntityList(): Single<List<NumberEntity>>

    /**
     * Get an [Single] which will emit a [NumberEntity] by its id.

     * @param id The id to retrieve user data.
     */
    fun numberEntityDetails(id: Long): Single<NumberEntity>
}