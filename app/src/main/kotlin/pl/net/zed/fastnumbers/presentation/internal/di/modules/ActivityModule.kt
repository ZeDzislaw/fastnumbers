package pl.net.zed.fastnumbers.presentation.internal.di.modules

import android.app.Activity
import dagger.Module
import dagger.Provides
import pl.net.zed.fastnumbers.presentation.internal.di.PerActivity


/**
 * Created by Damian Zawadzki on 17.01.2017.
 */
@Module
class ActivityModule(private val activity: Activity) {

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides @PerActivity internal fun activity(): Activity = this.activity
}